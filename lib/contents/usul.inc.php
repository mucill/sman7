<?php
/**
 * Copyright (C) 2015, Arie Nugraha (dicarve@gmail.com)
 * Custom by Eddy Subratha (eddy.subratha@slims.web.id)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

require_once SB.$sysconf['template']['dir'].'/'.$sysconf['template']['theme'].'/usulan_template.php';
$page_title = __('Usulan Buku');
$info       = '<h3>'.__('Usulan Pengadaan Buku').'</h3>';

if($_POST){
    if($_POST['Submit']=='Diusulkan'){
        $tanggal    = date('Y-m-d H:i:s');
        $nama       = $_POST['nama'];
        $judul      = $_POST['judul'];
        $pengarang  = $_POST['pengarang'];
        $penerbit   = $_POST['penerbit'];
        $alasan     = $_POST['alasan'];       
        $dbs->query("INSERT INTO usulan_buku (id, tanggal, nama, judul, pengarang, penerbit, alasan, status) VALUES (NULL, '".$tanggal."', '".$nama."', '".$judul."', '".$pengarang."', '".$penerbit."', '".$alasan."', 0)");
        utility::jsAlert("Usulan anda telah kami simpan. Terimakasih.");
    }
}

echo '<h3>Data Usulan Buku</h3>';
echo '<br>';
echo '<div><a href="index.php?p=daftar" class="xmlDetailLink">Klik disini untuk mengusulkan buku</a></div>';
echo '<br>';
$q            = $dbs->query("SELECT * FROM usulan_buku WHERE status=0 ORDER BY tanggal ASC");
$total_row    = $q->num_rows;
$per_page     = 10;
$num_page     = floor($total_row/$per_page)+1;
$cur_page     = 1;
$paging       = '';
if(isset($_GET['page'])) $cur_page = $_GET['page'];
$start_page   = ($cur_page - 1) * $per_page;
$end_page     = $start_page + $per_page;

$q_data       = $dbs->query("SELECT * FROM usulan_buku WHERE status = 0 ORDER BY tanggal ASC LIMIT $start_page, $per_page");

for($i=1; $i < $num_page; $i++){
    if($cur_page == $i) {
        $paging .= '<b>'.$i.'</b>&nbsp;';
    } else {
        $paging .= '&nbsp;<a href="index.php?p=usul&amp;page=' . $i . '">'.$i.'</a>&nbsp;';    
    }
}

ob_start();
while($data = $q_data->fetch_row() ) {
  echo " 
  <tr>
    <td style='padding:10px; text-align: right;'>".date_format(date_create($data[1]), 'd F Y')."</td>
    <td style='padding:10px'>".$data[3]."</td>
    <td style='padding:10px'>".$data[4]."</td>
    <td style='padding:10px'>".$data[5]."</td>
  </tr>";
}
$daftar_usulan = ob_get_clean();
?>

<div class="panel panel-default">
<table class="table table-bordered">
  <thead>
  <tr>
    <th>Diusulkan</th>
    <th>Judul</th>
    <th>Pengarang</th>
    <th>Penerbit</th>
  </tr>
  </thead>
  <?php echo $daftar_usulan ?>
</table>
</div>
<div class="row">
    <div class="col-md-6">
        <div>Hal.<?php echo $cur_page ?></div>
    </div>
    <div class="col-md-6 text-right">
        <div class="biblioPaging">
            <span class="pagingList"><?php echo $paging ?></span>
        </div>
    </div>
</div>