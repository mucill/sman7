<?php 

/*

VERSION : 3.0
CODENAME : SENAYAN
AUTHOR :
    Code and Programming : ARIE NUGRAHA (dicarve@yahoo.com)
    Database Design : HENDRO WICAKSONO (hendrowicaksono@yahoo.com) & WARDIYONO (wynerst@gmail.com)
    Customize : TOTO PRIYONO (toto_priyono@yahoo.com ) & Eddy Subratha (eddy.subratha@slims.web.id)

SENAYAN Library Automation System
Copyright (C) 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (GPL License.txt); if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* Reporting section */

// key to authenticate
define('INDEX_AUTH', '1');

// main system configuration
require '../../../../sysconfig.inc.php';

// IP based access limitation
require LIB.'ip_based_access.inc.php';
do_checkIP('smc');
do_checkIP('smc-reporting');

// start the session
require SB.'admin/default/session.inc.php';
require SB.'admin/default/session_check.inc.php';

// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.__('You don\'t have enough privileges to access this area!').'</div>');
}

$str="SELECT * FROM mst_member_type";
$query=$dbs->query($str);
$i=0;
while($data=$query->fetch_array()){
    $jenis_member[$i]['id']=$data['member_type_id'];
    $jenis_member[$i]['name']=$data['member_type_name'];
    $member_type[$data['member_type_id']]=$data['member_type_name'];
    $i++;
}

$tahun_sekarang=date('Y');
$start_tahun=$tahun_sekarang-14;
$str="select t2.member_type_id as member_type_id,t2.member_type_name as jenis_member,count(t1.member_id) as total, year(t1.loan_date) as tahun from loan as t1,mst_member_type as t2 , member as t3 where t1.member_id=t3.member_id and t2.member_type_id=t3.member_type_id and year(loan_date)>=$start_tahun and year(loan_date)<=$tahun_sekarang group by jenis_member,tahun  order by tahun,jenis_member asc";
$query=$dbs->query($str);
while($data=$query->fetch_array()){
    foreach($jenis_member as $item){
        if ($data['member_type_id']==$item['id']) { 
            $statistik_peminjam[$data['member_type_id']]['id']=$data['member_type_id'];
            $statistik_peminjam[$data['member_type_id']][$data['tahun']]=$data['total'];
        }
    }
}

?>

<fieldset>
    <div class="per_title">
      <h2><?php echo __('Statistik Jumlah Peminjam Periode 15 Tahun Terakhir'); ?></h2>
    </div>
    <div class="sub_section">
<table width="100%" id="dataList" cellpadding="5" cellspacing="0" >
    <thead>
    <tr class="dataListHeader" style="font-weight: bold; cursor: pointer; background-color: rgb(49, 53, 62);">
    <td>Jenis Member</td>
    <?php for($i=14;$i>=0;$i--){ ?>
        <td>
        <a style="color: yellow; " href="<?php echo MDL .DS ?>reporting/addons/statistik_peminjaman_perbulan.php?tahun=<?php echo date('Y')- $i ?>"><?php echo date('Y') - $i ?></a>
        </td>
    <?php } ?>
  </tr>
  </thead>
  <tbody>
  <?php $i=0; foreach($statistik_peminjam as $item) { ?>
  <tr>
    <td><?php echo $member_type[$item['id']]?></td>
    <td class="alterCell2"><?php echo @($item[date('Y')-14]=='')?0:$item[date('Y')-14]?></td>
    <td class="alterCell"><?php echo @($item[date('Y')-13]=='')?0:$item[date('Y')-13]?></td>
    <td class="alterCell2"><?php echo @($item[date('Y')-12]=='')?0:$item[date('Y')-12]?></td>
    <td class="alterCell"><?php echo @($item[date('Y')-11]=='')?0:$item[date('Y')-11]?></td>
    <td class="alterCell2"><?php echo @($item[date('Y')-10]=='')?0:$item[date('Y')-10]?></td>
    <td class="alterCell"><?php echo @($item[date('Y')-9]=='')?0:$item[date('Y')-9]?></td>
    <td class="alterCell2"><?php echo @($item[date('Y')-8]=='')?0:$item[date('Y')-8]?></td>
    <td class="alterCell"><?php echo @($item[date('Y')-7]=='')?0:$item[date('Y')-7]?></td>
    <td class="alterCell2"><?php echo @($item[date('Y')-6]=='')?0:$item[date('Y')-6]?></td>
    <td class="alterCell"><?php echo @($item[date('Y')-5]=='')?0:$item[date('Y')-5]?></td>
    <td class="alterCell2"><?php echo @($item[date('Y')-4]=='')?0:$item[date('Y')-4]?></td>
    <td class="alterCell"><?php echo @($item[date('Y')-3]=='')?0:$item[date('Y')-3]?></td>
    <td class="alterCell2"><?php echo @($item[date('Y')-2]=='')?0:$item[date('Y')-2]?></td>
    <td class="alterCell"><?php echo @($item[date('Y')-1]=='')?0:$item[date('Y')-1]?></td>
    <td class="alterCell2"><?php echo @($item[date('Y')]=='')?0:$item[date('Y')]?></td>
  </tr>
  <?php
    $i++;
  } 
  ?>
</tbody>
</table>
    </div>
</fieldset>
<iframe name="submitPrint" style="visibility: hidden; width: 0; height: 0;"></iframe>   
