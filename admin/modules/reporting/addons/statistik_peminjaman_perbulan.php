<?php 

/*

VERSION : 3.0
CODENAME : SENAYAN
AUTHOR :
    Code and Programming : ARIE NUGRAHA (dicarve@yahoo.com)
    Database Design : HENDRO WICAKSONO (hendrowicaksono@yahoo.com) & WARDIYONO (wynerst@gmail.com)
    Customize : TOTO PRIYONO (toto_priyono@yahoo.com ) & Eddy Subratha (eddy.subratha@slims.web.id)

SENAYAN Library Automation System
Copyright (C) 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (GPL License.txt); if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* Reporting section */

// key to authenticate
define('INDEX_AUTH', '1');

// main system configuration
require '../../../../sysconfig.inc.php';

// IP based access limitation
require LIB.'ip_based_access.inc.php';
do_checkIP('smc');
do_checkIP('smc-reporting');

// start the session
require SB.'admin/default/session.inc.php';
require SB.'admin/default/session_check.inc.php';

// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.__('You don\'t have enough privileges to access this area!').'</div>');
}

$str = "SELECT * FROM mst_member_type ORDER BY member_type_id ASC";
$query = $dbs->query($str);
while($data = $query->fetch_array()){
    $jenis_member[$data['member_type_id']]['id']=$data['member_type_id'];
    $jenis_member[$data['member_type_id']]['name']=$data['member_type_name'];
}

$data_statistik = array();
$bulan=array('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des');
$tahun=$_GET['tahun'];
$str="  SELECT 
            t2.member_type_id AS member_type_id,
            t2.member_type_name AS jenis_member, 
            COUNT(t1.member_id) AS total, 
            month(t1.loan_date) AS bulan 
        FROM 
            loan AS t1,mst_member_type AS t2, member AS t3 
        WHERE 
            t1.member_id=t3.member_id 
            AND t2.member_type_id=t3.member_type_id 
            AND year(loan_date)= $tahun 
        GROUP BY 
            jenis_member,bulan  
        ORDER BY bulan, jenis_member ASC";

$query=$dbs->query($str);
while($data=$query->fetch_array()){
    foreach ($jenis_member as $item){
        if($data['member_type_id']==$item['id']) {
            $data_statistik[$item['id']][$data['bulan']]=$data['total']; 
            $data_statistik[$item['id']]['id']=$item['id'];
        }
    }
}

?>


<fieldset>
    <div class="per_title">
      <h2><?php echo __('Jumlah Peminjaman Koleksi Tahun '.$tahun) ?></h2>
    </div>
    <div class="sub_section">
        <a class="button" href="<?php echo MDL .DS ?>reporting/addons/statistik_peminjaman.php">Kembali ke Data Per Tahun</a>
        <br><br>
        <table width="100%" id="dataList" cellpadding="5" cellspacing="0" >
            <thead>
            <tr class="dataListHeader" style="font-weight: bold; cursor: pointer; background-color: rgb(49, 53, 62);">
            <td>Jenis Member</td>
            <?php for($i=1; $i<=12; $i++){ ?>
                <td><?php echo $bulan[$i-1] ?> </td>
            <?php } ?>
            </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                foreach($data_statistik as $item) { 
                ?>
                <tr>
                    <td><?php echo $jenis_member[$item['id']]['name']?></td>
                    <td class="alterCell"><?php echo @($item[1]=='')?0:$item[1]?></td>
                    <td class="alterCell2"><?php echo @($item[2]=='')?0:$item[2]?></td>
                    <td class="alterCell"><?php echo @($item[3]=='')?0:$item[3]?></td>
                    <td class="alterCell2"><?php echo @($item[4]=='')?0:$item[4]?></td>
                    <td class="alterCell"><?php echo @($item[5]=='')?0:$item[5]?></td>
                    <td class="alterCell2"><?php echo @($item[6]=='')?0:$item[6]?></td>
                    <td class="alterCell"><?php echo @($item[7]=='')?0:$item[7]?></td>
                    <td class="alterCell2"><?php echo @($item[8]=='')?0:$item[8]?></td>
                    <td class="alterCell"><?php echo @($item[9]=='')?0:$item[9]?></td>
                    <td class="alterCell2"><?php echo @($item[10]=='')?0:$item[10]?></td>
                    <td class="alterCell"><?php echo @($item[11]=='')?0:$item[11]?></td>
                    <td class="alterCell2"><?php echo @($item[12]=='')?0:$item[12]?></td>
                </tr>
                <?php $i++; }  ?>
            </tbody>
        </table>
    </div>


</fieldset>
<iframe name="submitPrint" style="visibility: hidden; width: 0; height: 0;"></iframe>   
