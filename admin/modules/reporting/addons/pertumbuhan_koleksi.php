<?php 

/*

VERSION : 3.0
CODENAME : SENAYAN
AUTHOR :
    Code and Programming : ARIE NUGRAHA (dicarve@yahoo.com)
    Database Design : HENDRO WICAKSONO (hendrowicaksono@yahoo.com) & WARDIYONO (wynerst@gmail.com)
    Customize : TOTO PRIYONO (toto_priyono@yahoo.com ) & Eddy Subratha (eddy.subratha@slims.web.id)

SENAYAN Library Automation System
Copyright (C) 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (GPL License.txt); if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* Reporting section */

// key to authenticate
define('INDEX_AUTH', '1');

// main system configuration
require '../../../../sysconfig.inc.php';

// IP based access limitation
require LIB.'ip_based_access.inc.php';
do_checkIP('smc');
do_checkIP('smc-reporting');

// start the session
require SB.'admin/default/session.inc.php';
require SB.'admin/default/session_check.inc.php';

// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.__('You don\'t have enough privileges to access this area!').'</div>');
}

$reportView = false;
if (isset($_GET['reportView'])) {
    $reportView = true;
}

if (!$reportView) {
    echo '<iframe name="reportView" id="reportView" src="'.$_SERVER['PHP_SELF'].'?reportView=true" frameborder="0" style="width: 100%; height: 800px;"></iframe>';
} else {

    $sumber         = array('Paket','Pembelian','Hadiah','Pelajaran');
    $tahun_sekarang = date('Y');
    $start_tahun    = $tahun_sekarang-14;
    $str            = " SELECT 
                            source, 
                            COUNT(item_id) AS total, 
                            year(received_date) AS tahun  
                        FROM item 
                        WHERE 
                            year(received_date) >= $start_tahun 
                            AND year(received_date) <= $tahun_sekarang 
                        GROUP BY source, tahun 
                        ORDER BY tahun, source ASC";
    $query          = $dbs->query($str);

    while($data = $query->fetch_array()){
        if($data['source'] == 1) $source[1][$data['tahun']] = $data['total'];
        if($data['source'] == 2) $source[2][$data['tahun']] = $data['total'];
        if($data['source'] == 3) $source[3][$data['tahun']] = $data['total'];
        if($data['source'] == 4) $source[4][$data['tahun']] = $data['total'];
    }


    $str_type_id="  SELECT 
                        coll_type_id,
                        coll_type_name 
                    FROM mst_coll_type 
                    ORDER BY coll_type_id ASC";
    $query = $dbs->query($str_type_id);
    while($data=$query->fetch_array()){
        $jenis_koleksi[$data['coll_type_id']]['id']     = $data['coll_type_id'];
        $jenis_koleksi[$data['coll_type_id']]['name']   = $data['coll_type_name'];
    }

    $str = "SELECT 
                coll_type_id AS type_id,
                COUNT(item_id) AS total,
                year(received_date) AS tahun
            FROM item 
            WHERE year(received_date) >= $start_tahun 
                AND year(received_date) <= $tahun_sekarang 
            GROUP BY type_id, tahun 
            ORDER BY tahun, source ASC";

    $query = $dbs->query($str);
    while($data = $query->fetch_array()) {
        foreach($jenis_koleksi as $item){
            if($data['type_id'] == $item['id']) { 
                $jenis[$item['name']][$data['tahun']]   = $data['total']; 
                $jenis[$item['name']]['id']             = $data['type_id']; 
            }
        }
    }
    
    ob_start();
?>
<fieldset>
    <div class="per_title">
      <h2><?php echo __('Pertambahan Koleksi'); ?></h2>
    </div>
    <div class="sub_section">
    <h4>Berdasarkan Asal atau Sumber Koleksi 15 Tahun Terakhir</h4>
    <table width="100%" id="dataListPrinted" cellpadding="5" cellspacing="0" >
        <thead>
        <tr class="dataListHeader" style="font-weight: bold; cursor: pointer; background-color: rgb(49, 53, 62);">
        <td>Asal Koleksi</td>
        <?php for($i=14; $i>=0; $i--){ ?>
        <td>
            <a style="color: yellow" href="pertumbuhan_asal_perbulan.php?tahun=<?php echo (date('Y')-$i)?>"><?php echo (date('Y')-$i)?></a>
        </td>
        <?php } ?>
      </tr>
      </thead>
      <tbody>
      <?php $i=0; foreach($source as $item) { ?>
          <tr>
            <td><?php echo $sumber[$i]?></td>
            <td class="data"><?php echo @($item[date('Y')-14]=='')?0:$item[date('Y')-14]?></td>
            <td class="data"><?php echo @($item[date('Y')-13]=='')?0:$item[date('Y')-13]?></td>
            <td class="data"><?php echo @($item[date('Y')-12]=='')?0:$item[date('Y')-12]?></td>
            <td class="data"><?php echo @($item[date('Y')-11]=='')?0:$item[date('Y')-11]?></td>
            <td class="data"><?php echo @($item[date('Y')-10]=='')?0:$item[date('Y')-10]?></td>
            <td class="data"><?php echo @($item[date('Y')-9]=='')?0:$item[date('Y')-9]?></td>
            <td class="data"><?php echo @($item[date('Y')-8]=='')?0:$item[date('Y')-8]?></td>
            <td class="data"><?php echo @($item[date('Y')-7]=='')?0:$item[date('Y')-7]?></td>
            <td class="data"><?php echo @($item[date('Y')-6]=='')?0:$item[date('Y')-6]?></td>
            <td class="data"><?php echo @($item[date('Y')-5]=='')?0:$item[date('Y')-5]?></td>
            <td class="data"><?php echo @($item[date('Y')-4]=='')?0:$item[date('Y')-4]?></td>
            <td class="data"><?php echo @($item[date('Y')-3]=='')?0:$item[date('Y')-3]?></td>
            <td class="data"><?php echo @($item[date('Y')-2]=='')?0:$item[date('Y')-2]?></td>
            <td class="data"><?php echo @($item[date('Y')-1]=='')?0:$item[date('Y')-1]?></td>
            <td class="data"><?php echo @($item[date('Y')]=='')?0:$item[date('Y')]?></td>
          </tr>

      <?php $i++; } ?>
    </tbody>
    </table>
    </div>

    <div class="sub_section">
    <h4>Berdasarkan Jenis Koleksi 15 Tahun Terakhir</h4>
    <table width="100%" id="dataListPrinted" cellpadding="5" cellspacing="0" >
        <thead>
        <tr class="dataListHeader" style="font-weight: bold; cursor: pointer; background-color: rgb(49, 53, 62);">
        <td>Asal Koleksi</td>
        <?php for($i=14; $i>=0; $i--){ ?>
        <td>
            <a style="color: yellow" href="pertumbuhan_jenis_perbulan.php?tahun=<?php echo (date('Y')-$i) ?>"><?php echo (date('Y')-$i)?></a>
        </td>
        <?php } ?>
      </tr>
      </thead>
      <tbody>
      <?php $i=0; foreach($jenis as $item) { ?>
      <tr>
        <td><?php echo $jenis_koleksi[$item['id']]['name']?></td>
        <td class="data"><?php echo @($item[date('Y')-14]=='')?0:$item[date('Y')-14]?></td>
        <td class="data"><?php echo @($item[date('Y')-13]=='')?0:$item[date('Y')-13]?></td>
        <td class="data"><?php echo @($item[date('Y')-12]=='')?0:$item[date('Y')-12]?></td>
        <td class="data"><?php echo @($item[date('Y')-11]=='')?0:$item[date('Y')-11]?></td>
        <td class="data"><?php echo @($item[date('Y')-10]=='')?0:$item[date('Y')-10]?></td>
        <td class="data"><?php echo @($item[date('Y')-9]=='')?0:$item[date('Y')-9]?></td>
        <td class="data"><?php echo @($item[date('Y')-8]=='')?0:$item[date('Y')-8]?></td>
        <td class="data"><?php echo @($item[date('Y')-7]=='')?0:$item[date('Y')-7]?></td>
        <td class="data"><?php echo @($item[date('Y')-6]=='')?0:$item[date('Y')-6]?></td>
        <td class="data"><?php echo @($item[date('Y')-5]=='')?0:$item[date('Y')-5]?></td>
        <td class="data"><?php echo @($item[date('Y')-4]=='')?0:$item[date('Y')-4]?></td>
        <td class="data"><?php echo @($item[date('Y')-3]=='')?0:$item[date('Y')-3]?></td>
        <td class="data"><?php echo @($item[date('Y')-2]=='')?0:$item[date('Y')-2]?></td>
        <td class="data"><?php echo @($item[date('Y')-1]=='')?0:$item[date('Y')-1]?></td>
        <td class="data"><?php echo @($item[date('Y')]=='')?0:$item[date('Y')]?></td>
      </tr>
      <?php $i++; } ?>
    </tbody>
    </table>
    <br>
    <a class="printReport button" onclick="window.print()" href="#">Print Current Page</a>    
    <br>
    <br> 
    </div>

</fieldset>
<?php 
$content = ob_get_clean();
// include the page template
require SB.'/admin/'.$sysconf['admin_template']['dir'].'/printed_page_tpl.php';
}
?>