<?php
/**
 * Copyright (C) 2007,2008  Arie Nugraha (dicarve@yahoo.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

// be sure that this file not accessed directly
if (INDEX_AUTH != 1) {
    die("can not access this file directly");
}

/* Custom reports list */

$menu[] = array(__('Rekap per Klasifikasi'), MWB.'reporting/addons/keadaan_koleksi_klass.php', __('Rekap per Klasifikasi'));
$menu[] = array(__('Rekap Per Jenis Koleksi'), MWB.'reporting/addons/keadaan_koleksi_jenis.php', __('Rekap Per Jenis Koleksi'));
$menu[] = array(__('Daftar Koleksi'), MWB.'reporting/addons/daftar_koleksi.php', __('Daftar Koleksi'));
$menu[] = array(__('Frekuensi Peminjaman'), MWB.'reporting/addons/frek_peminjaman.php', __('Frekuensi Peminjaman'));
$menu[] = array(__('Statistik Peminjaman'), MWB.'reporting/addons/statistik_peminjaman.php', __('Statsitik Peminjaman'));
$menu[] = array(__('Daftar Denda'), MWB.'reporting/addons/daftar_denda.php', __('Daftar Denda'));
$menu[] = array(__('Pertambahan Koleksi'), MWB.'reporting/addons/pertumbuhan_koleksi.php', __('Pertambahan Koleksi'));
$menu[] = array(__('Usulan Buku (Belum diproses)'), MWB.'reporting/addons/daftar_usulan.php', __('Usulan Buku (Belum diproses)'));
$menu[] = array(__('Usulan Buku (Sudah diproses)'), MWB.'reporting/addons/daftar_usulan_proses.php', __('Usulan Buku (Sudah diproses)'));