<?php

// start the session
session_start();

require '../../../../sysconfig.inc.php';
require SIMBIO_BASE_DIR.'simbio_GUI/table/simbio_table.inc.php';

//print_r($_POST);
// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.lang_sys_common_no_privilage.'</div>');
}

?>

<style type="text/css">
.garis-header {
	border-bottom:2px solid #ccc;
	font-weight:bold;
	text-align:center;
	
}

.garis-content {
border-bottom:1px solid #ccc;
text-align:left;
padding-right:5px;
}

.garis-content2 {
border-bottom:1px solid #ccc;
text-align:right;
padding-right:5px;
}

.tableDataGrid{
	float:left;
	width:90%;
	height:50px;
	overflow:scroll;
	border:1px solid #fff;;
}

.frmCari{
border:1px solid #999;

}

</style>
<h2>Data Denda Detail untuk Tanggal <?=$_GET['tanggal']?></h2>
<br>
<table class="tableDataGrid" width="90%">
<tr class="dataListHeader">
<td colspan="7">Data Denda</td>
</tr>
<tr>
	<td width="10%" class="garis-header">Tanggal</td>
	<td width="20%" class="garis-header">Member ID</td>
	<td width="30%" class="garis-header">Nama Member</td>
	<td width="20%" class="garis-header">Jumlah Denda</td>
	<td width="20%" class="garis-header">Jumlah Bayar</td>
</tr>
<?
//xx-xx-yyyy
$tahun=substr($_GET['tanggal'],6,4);
$bulan=substr($_GET['tanggal'],3,2);
$tanggal=substr($_GET['tanggal'],0,2);

$tgl="$tahun-$bulan-$tanggal";
$str="select t2.member_name,t1.* from fines as t1 , member as t2 where t1.member_id=t2.member_id and t1.fines_date='".$tgl."'";
$query=$dbs->query($str);
while($data=$query->fetch_array()){ ?>
<tr>
	<td class='garis-content'><?=$data['fines_date']?></td>
	<td class='garis-content'><?=$data['member_id']?></td>
	<td class='garis-content'><?=$data['member_name']?></td>
	<td class='garis-content2'><?=$data['debet']?></td>
	<td class='garis-content2'><?=$data['kredit']?></td>

</tr>
<?
}
?>
</table>
