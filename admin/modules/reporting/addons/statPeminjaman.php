<?php

// start the session
session_start();

require '../../../sysconfig.inc.php';
require SIMBIO_BASE_DIR.'simbio_GUI/table/simbio_table.inc.php';

// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.lang_sys_common_no_privilage.'</div>');
}
?>

<style type="text/css">
.garis-header {
	border-bottom:2px solid #ccc;
	font-weight:bold;
	
}

.garis-content {
border-bottom:1px solid #ccc;
}

.tableDataGrid{
	float:left;
	width:100%;
	height:50px;
	overflow:scroll;
	border:1px solid #fff;;
}

</style>


<table id="searchForm" cellpadding="5" cellspacing="0">
<tr>
    <td class="imageLeft" valign="top" style="background-image: url(<?php echo $sysconf['admin_template']['dir'].'/'.$sysconf['admin_template']['theme'].'/statistic.png'; ?>)">
        <?php echo strtoupper(lang_mod_report_stat); ?>
                <hr />
                <form name="printForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" target="submitPrint" id="printForm" id="printForm" method="get" style="display: inline;">
        <input type="hidden" name="print" value="true" /><input type="submit" value="<?php echo lang_sys_common_form_report; ?>" class="button" />
                </form>
                <iframe name="submitPrint" style="visibility: hidden; width: 0; height: 0;"></iframe>
    </td>
</tr>
</table>

<table width="100%"  border="0">
  <tr>
    <td>
		<form id="frmCari" method="post" action="" onSubmit="$('cari').click();">       
	<table width="100%"  class="frmCari">
        <tr>
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0" style="background-color:#DDD ">
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="holder_start">&nbsp;</td>
            </tr>
            <tr>
              <td width="6%">&nbsp;</td>
              <td width="10%">Dari </td>
              <td width="84%" id="holder_start"><input type="text" name="start_date" value="" onFocus="showCalendar('', this, this, '<?=date("Y-m-d")?>', 'holder_start', 0,30, 1)"> 
               </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Sampai</td>
              <td width="72%" id="holder_end"><input type="text" name="end_date" value="" onFocus="showCalendar('', this, this, '<?=date("Y-m-d")?>', 'holder_end', 0,30, 1)"> 
                 </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="button" name="Submit" id="cari" value="Cari" onclick="setContent('mainContent', '<?php echo MODULES_WEB_ROOT_DIR; ?>reporting/statPeminjaman.php', 'post', $('frmCari').serialize(), true);"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table></td>
        </tr>
      </table>
	  </form>      
	</td>
  </tr>
</table>

<h3>Periode <?=$_POST['start_date']?> - <?=$_POST['end_date']?></h3>
<table class="tableDataGrid" width="100%">
<tr class="dataListHeader">
<td colspan="7">Data Frekuensi Peminjaman</td>
</tr>
<tr>
	<td width="4%" class="garis-header">No</td>
	<td width="10%" class="garis-header" >MemberID</td>
	<td width="24%" class="garis-header">Nama</td>
	<td width="19%" class="garis-header">Kelas</td>
	<td width="5%" class="garis-header">Frek Pinjam</td>
</tr>

<?
if($_POST){
	if(!empty($_POST['start_date'])&&!empty($_POST['end_date'])){

		$str="select t1.member_id,t1.member_name,t1.inst_name,count(t2.member_id)as jml from loan as t2 , member as t1 where t1.member_id=t2.member_id and t2.loan_date>='".$_POST['start_date']."' and t2.loan_date<='".$_POST['end_date']."' group by t2.member_id order by jml desc limit 10";
		$query=$dbs->query($str);
		
		$no=0;
		while($data=$query->fetch_array()){
		echo"
		<tr>
			<td class='garis-content'>".++$no."</td>
			<td class='garis-content'>".$data['member_id']."</td>
			<td class='garis-content'>".$data['member_name']."</td>
			<td class='garis-content'>".$data['inst_name']."</td>
			<td class='garis-content'>".$data['jml']."</td>
		</tr>";
		}
	}
}
?>

</table>
