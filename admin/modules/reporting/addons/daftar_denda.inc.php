<?php

// start the session
session_start();

require '../../../../sysconfig.inc.php';
require SIMBIO_BASE_DIR.'simbio_GUI/table/simbio_table.inc.php';

//print_r($_POST);
// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.lang_sys_common_no_privilage.'</div>');
}

?>

<style type="text/css">
.garis-header {
	border-bottom:2px solid #ccc;
	font-weight:bold;
	text-align:center;
	
}

.garis-content {
border-bottom:1px solid #ccc;
text-align:right;
padding-right:5px;
}

.tableDataGrid{
	float:left;
	width:70%;
	height:50px;
	overflow:scroll;
	border:1px solid #fff;;
}

.frmCari{
border:1px solid #999;

}

</style>

<table width="100%"  border="0">
  <tr>
    <td>
		<form id="frmCari" method="post" action="" onSubmit="$('cari').click();">       
	<table width="100%"  class="frmCari">
        <tr>
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0" style="background-color:#DDD ">
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td id="holder_start">&nbsp;</td>
            </tr>
            <tr>
              <td width="6%">&nbsp;</td>
              <td width="10%">Dari </td>
              <td width="84%" id="holder_start"><input type="text" name="start_date" value="" onFocus="showCalendar('', this, this, '<?=date("Y-m-d")?>', 'holder_start', 0,30, 1)"> 
              ( yyyy/mm/dd ) </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Sampai</td>
              <td width="72%" id="holder_end"><input type="text" name="end_date" value="" onFocus="showCalendar('', this, this, '<?=date("Y-m-d")?>', 'holder_end', 0,30, 1)"> 
                (  
                yyyy/mm/dd ) </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="button" name="Submit" id="cari" value="Cari" onclick="setContent('mainContent', '<?php echo MODULES_WEB_ROOT_DIR; ?>reporting/customs/daftar_denda.inc.php', 'post', $('frmCari').serialize(), true);"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table></td>
        </tr>
      </table>
	  </form>      
	</td>
  </tr>
</table>

<table class="tableDataGrid" width="50%">
<tr class="dataListHeader">
<td colspan="7">Data Denda</td>
</tr>
<tr>
	<td width="10%" class="garis-header">Tanggal</td>
	<td width="30%" class="garis-header">Jumlah yg didenda</td>
	<td width="30%" class="garis-header">Jumlah Denda</td>
	<td width="30%" class="garis-header">Jumlah Bayar</td>
</tr>
<?
$str="select date_format(fines_date,'%d-%m-%Y') as tanggal,count(fines_id) as jumlah, sum(debet) as denda, sum(credit) as terbayar from fines  ";
$extra_str="";
if($_POST){
	if(!empty($_POST['start_date'])&&!empty($_POST['end_date'])){
			$extra_str=" where fines_date >='".$_POST['start_date']."' and fines_date<='".$_POST['end_date']."'";		
	}
	
$str_orderby=" group by fines_date";
$str=$str.$extra_str.$str_orderby;
$query=$dbs->query($str);

$no=0;
while($data=$query->fetch_row()){ ?>
<tr>
	<td class='garis-content'>
		<a onclick="setContent('mainContent', '<?php echo MODULES_WEB_ROOT_DIR; ?>reporting/customs/daftar_denda_detail.php?tanggal=<?=$data[0]?>', 'get');"
		href="#"><?=$data[0]?></a>
	</td>
	<td class='garis-content'><?=$data[1]?></td>
	<td class='garis-content'><?=$data[2]?></td>
	<td class='garis-content'><?=$data[3]?></td>
</tr>

<?
}


}
?>


</table>
