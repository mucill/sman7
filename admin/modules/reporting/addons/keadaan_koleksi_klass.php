<?php

/*

VERSION : 3.0
CODENAME : SENAYAN
AUTHOR :
    Code and Programming : ARIE NUGRAHA (dicarve@yahoo.com)
    Database Design : HENDRO WICAKSONO (hendrowicaksono@yahoo.com) & WARDIYONO (wynerst@gmail.com)
	Customize : TOTO PRIYONO (toto_priyono@yahoo.com ) & Eddy Subratha (eddy.subratha@slims.web.id)

SENAYAN Library Automation System
Copyright (C) 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (GPL License.txt); if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* Reporting section */

// key to authenticate
define('INDEX_AUTH', '1');

// main system configuration
require '../../../../sysconfig.inc.php';

// IP based access limitation
require LIB.'ip_based_access.inc.php';
do_checkIP('smc');
do_checkIP('smc-reporting');

// start the session
require SB.'admin/default/session.inc.php';
require SB.'admin/default/session_check.inc.php';

// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.__('You don\'t have enough privileges to access this area!').'</div>');
}

$bulan =array(
		'1'=>'Januari',
		'2'=>'Februari',
		'3'=>'Maret',
		'4'=>'April',
		'5'=>'Mei',
		'6'=>'Juni',
		'7'=>'Juli',
		'8'=>'Agustus',
		'9'=>'September',
		'10'=>'Oktober',
		'11'=>'November',
		'12'=>'Desember'
		);


// total number of titles

// if we are in print mode
if (isset($_GET['print'])) {
	$class_query = $dbs->query('SELECT class_number,class_name FROM klasifikasi WHERE level=1');

    // html strings
	$html_str = '<html><head><title>Laporan Keadaan Koleksi</title>';
	$html_str .= '<style type="text/css">';
	$html_str .= '.title_report{ font-size:12px; text-align:center;}';
	$html_str .= '.report{ border:2px solid #000; font-family:Arial, Helvetica, sans-serif;font-size:11px; }';
	$html_str .= '.report tr.header{ font-size:12px;text-align:center;}';
	$html_str .= '.report tr.header td{border-bottom:1px solid #000;border-left:1px solid #000;text-align:center;}';
	$html_str .= '.report td{border-bottom:1px solid #000;border-left:1px solid #000;text-align:right; padding-right:2px;}';
	$html_str .= '.report tr.total td{border-top:2px solid #000;text-align:right; padding-right:2px;}';
	$html_str .= '</style>';
	$html_str .= '</head>';
	$html_str .= '<body>';
    $html_str .= '<div class="title_report">Sekolah Menengah Atas Negeri 7 Purworejo<br>Jl. Ki Mangunsarkoro No. 1 Purworejo<br><br>Laporan Keadaan Koleksi Berdasarkan Klasifikasi<br>Per Bulan: '.$bulan[$_GET['bulan']].' '.$_GET['tahun'].'<br><br>';
	$html_str .= 'Tanggal Cetak :'.date('l, d/m/Y H:i:s')." WIB </div>";
    $html_str .= '<hr size="1" />';
	$html_str .= '<table width="100%"  border="0" class="report" cellpadding="0" cellspacing="0">';
	$html_str .= '  <tr class="header">';
	$html_str .= '	<td width="3%" rowspan="2">No</td>';
	$html_str .= '	<td width="26%" rowspan="2">Klasifikasi</td>';
	$html_str .= '	<td colspan="4">Judul Buku </td>';
	$html_str .= '	<td colspan="4">Jumlah Eks </td>';
	$html_str .= '	<td colspan="4">Tambah Judul </td>';
	$html_str .= '	<td colspan="4">Tambah Eks </td>';
	$html_str .= '	<td colspan="2">Jumlah</td>';
	$html_str .= '	<td colspan="4">Hilang/Rusak [Judul]</td>';
	$html_str .= '	<td colspan="4">Hilang/Rusak [Eks]</td>';
	$html_str .= '	<td colspan="2">Jumlah</td>';
	$html_str .= '  </tr>';
	$html_str .= '  <tr class="header">';
	$html_str .= '	<td width="3%">1</td>';
	$html_str .= '	<td width="3%">2</td>';
	$html_str .= '	<td width="3%">3</td>';
	$html_str .= '	<td width="4%">4</td>';
	$html_str .= '	<td width="3%">1</td>';
	$html_str .= '	<td width="3%">2</td>';
	$html_str .= '	<td width="3%">3</td>';
	$html_str .= '	<td width="3%">4</td>';
	$html_str .= '	<td width="3%">1</td>';
	$html_str .= '	<td width="3%">2</td>';
	$html_str .= '	<td width="3%">3</td>';
	$html_str .= '	<td width="3%">4</td>';
	$html_str .= '	<td width="3%">1</td>';
	$html_str .= '	<td width="3%">2</td>';
	$html_str .= '	<td width="3%">3</td>';
	$html_str .= '	<td width="3%">4</td>';
	$html_str .= '	<td width="5%">Judul</td>';
	$html_str .= '	<td width="4%">Eks</td>';
	$html_str .= '	<td width="3%">1</td>';
	$html_str .= '	<td width="3%">2</td>';
	$html_str .= '	<td width="3%">3</td>';
	$html_str .= '	<td width="3%">4</td>';
	$html_str .= '	<td width="3%">1</td>';
	$html_str .= '	<td width="3%">2</td>';
	$html_str .= '	<td width="3%">3</td>';
	$html_str .= '	<td width="3%">4</td>';
	$html_str .= '	<td width="5%">Judul</td>';
	$html_str .= '	<td width="4%">Eks</td>';
	$html_str .= '  </tr>';

	$no=0;
	while($class_data=$class_query->fetch_array()){
	
	//hitung jumlah judul buku per klasifikasi per gmd_id hinggga bulan-1 dari bulan dan tahun yang dipilih;
	
	$sql="select t1.biblio_id ,t1.source from item as t1,biblio as t2 where t1.received_date < '".$_GET['tahun']."-".$_GET['bulan']."-01' and t2.classification_no like '".$class_data['class_number']."%' and t1.biblio_id=t2.biblio_id group by t1.biblio_id,t1.source order by t1.source asc";
	$title_query=$dbs->query($sql);
	$data_jumlah_judul[1]=$data_jumlah_judul[2]=$data_jumlah_judul[3]=$data_jumlah_judul[4]=0;
	while($title_data=$title_query->fetch_array()){
		if($title_data['source']==1) $data_jumlah_judul[1]=$data_jumlah_judul[1]+1;
		if($title_data['source']==2) $data_jumlah_judul[2]=$data_jumlah_judul[2]+1;
		if($title_data['source']==3) $data_jumlah_judul[3]=$data_jumlah_judul[3]+1;
		if($title_data['source']==4) $data_jumlah_judul[4]=$data_jumlah_judul[4]+1;
	}

	$total_judul[1]=$total_judul[1]+$data_jumlah_judul[1];
	$total_judul[2]=$total_judul[2]+$data_jumlah_judul[2];
	$total_judul[3]=$total_judul[3]+$data_jumlah_judul[3];
	$total_judul[4]=$total_judul[4]+$data_jumlah_judul[4];
	
	$total_judul_perklass=$grand_total_judul=0;	
	$total_judul_perklass=$data_jumlah_judul[1]+$data_jumlah_judul[2]+$data_jumlah_judul[3]+$data_jumlah_judul[4];
	$grand_total_judul=$total_judul[1]+$total_judul[2]+$total_judul[3]+$total_judul[4];
	    

	//hitung jumlah eksemplar buku per klasifikasi per gmd_id hinggga bulan-1 dari bulan dan tahun yang dipilih;
	$sql="select t1.biblio_id,t1.source from item as t1,biblio as t2 where t1.received_date<'".$_GET['tahun']."-".$_GET['bulan']."-01' and t2.classification_no like '".$class_data['class_number']."%' and t1.biblio_id=t2.biblio_id";
	$eks_query=$dbs->query($sql);
	$data_jumlah_eks[1]=$data_jumlah_eks[2]=$data_jumlah_eks[3]=$data_jumlah_eks[4]=0;
	while($eks_data=$eks_query->fetch_array()){
		if($eks_data['source']==1) $data_jumlah_eks[1]=$data_jumlah_eks[1]+1;
		if($eks_data['source']==2) $data_jumlah_eks[2]=$data_jumlah_eks[2]+1;
		if($eks_data['source']==3) $data_jumlah_eks[3]=$data_jumlah_eks[3]+1;
		if($eks_data['source']==4) $data_jumlah_eks[4]=$data_jumlah_eks[4]+1;
	}

	$total_eks[1]=$total_eks[1]+$data_jumlah_eks[1];
	$total_eks[2]=$total_eks[2]+$data_jumlah_eks[2];
	$total_eks[3]=$total_eks[3]+$data_jumlah_eks[3];
	$total_eks[4]=$total_eks[4]+$data_jumlah_eks[4];
	
	$total_eks_perklass=$grand_total_eks=0;	
	$total_eks_perklass=$data_jumlah_eks[1]+$data_jumlah_eks[2]+$data_jumlah_eks[3]+$data_jumlah_eks[4];


	//count koleksi rusak per judul
	$sql="select count(t1.biblio_id) as jml, t1.source  from item as t1,biblio as t2 where t1.received_date < '".$_GET['tahun']."-".$_GET['bulan']."-01' and t2.classification_no like '".$class_data['class_number']."%' and t1.item_status_id='R' and t1.biblio_id=t2.biblio_id group by t1.biblio_id,source";
	$rusak_judul_query=$dbs->query($sql);
	$data_jumlah_rusak_judul[1]=$data_jumlah_rusak_judul[2]=$data_jumlah_rusak_judul[3]=$data_jumlah_rusak_judul[4]=0;
	while($judul_data_rusak=$rusak_judul_query->fetch_array()){
		if($judul_data_rusak['source']==1) $data_jumlah_rusak_judul[1]=$data_jumlah_rusak_judul[1]+1;
		if($judul_data_rusak['source']==2) $data_jumlah_rusak_judul[2]=$data_jumlah_rusak_judul[2]+1;
		if($judul_data_rusak['source']==3) $data_jumlah_rusak_judul[3]=$data_jumlah_rusak_judul[3]+1;
		if($judul_data_rusak['source']==4) $data_jumlah_rusak_judul[4]=$data_jumlah_rusak_judul[4]+1;
	}

	$total_judul_rusak[1]=$total_judul_rusak[1]+$data_jumlah_rusak_judul[1];
	$total_judul_rusak[2]=$total_judul_rusak[2]+$data_jumlah_rusak_judul[2];
	$total_judul_rusak[3]=$total_judul_rusak[3]+$data_jumlah_rusak_judul[3];
	$total_judul_rusak[4]=$total_judul_rusak[4]+$data_jumlah_rusak_judul[4];
	
	$grand_total_judul_rusak=0;
	$grand_total_judul_rusak=$data_jumlah_rusak_judul[1]+$data_jumlah_rusak_judul[2]+$data_jumlah_rusak_judul[3]+$data_jumlah_rusak_judul[4];

	//count koleksi rusak per eksemplar
	$sql="select t1.biblio_id, t1.source  from item as t1,biblio as t2 where t1.received_date <'".$_GET['tahun']."-".$_GET['bulan']."-01' and t1.item_status_id='R' and t2.classification_no like '".$class_data['class_number']."%' and t1.biblio_id=t2.biblio_id";
	$rusak_eks_query=$dbs->query($sql);
	$data_jumlah_rusak_eks[1]=$data_jumlah_rusak_eks[2]=$data_jumlah_rusak_eks[3]=$data_jumlah_rusak_eks[4]=0;
	while($eks_data_rusak=$rusak_eks_query->fetch_array()){
		if($eks_data_rusak['source']==1) $data_jumlah_rusak_eks[1]=$data_jumlah_rusak_eks[1]+1;
		if($eks_data_rusak['source']==2) $data_jumlah_rusak_eks[2]=$data_jumlah_rusak_eks[2]+1;
		if($eks_data_rusak['source']==3) $data_jumlah_rusak_eks[3]=$data_jumlah_rusak_eks[3]+1;
		if($eks_data_rusak['source']==4) $data_jumlah_rusak_eks[4]=$data_jumlah_rusak_eks[4]+1;
	}

	$total_eks_rusak[1]=$total_eks_rusak[1]+$data_jumlah_rusak_eks[1];
	$total_eks_rusak[2]=$total_eks_rusak[2]+$data_jumlah_rusak_eks[2];
	$total_eks_rusak[3]=$total_eks_rusak[3]+$data_jumlah_rusak_eks[3];
	$total_eks_rusak[4]=$total_eks_rusak[4]+$data_jumlah_rusak_eks[4];
	
	$grand_total_eks_rusak=0;
	$grand_total_eks_rusak=$data_jumlah_rusak_eks[1]+$data_jumlah_rusak_eks[2]+$data_jumlah_rusak_eks[3]+$data_jumlah_rusak_eks[4];
	
	//count tambahan judul buku per klasifikasi per gmd_id;
	$sql="select t1.biblio_id ,t1.source from item as t1,biblio as t2 where t2.classification_no like '".$class_data['class_number']."%' and month(t1.received_date)='".$_GET['bulan']."' and year(t1.received_date)='".$_GET['tahun']."' and t1.biblio_id=t2.biblio_id group by t1.biblio_id order by t1.source asc";
	$add_query=$dbs->query($sql);
	$pertambahan_judul[1]=$pertambahan_judul[2]=$pertambahan_judul[3]=$pertambahan_judul[4]=0;
	while($add_data=$add_query->fetch_array()){
		if($add_data['source']==1) $pertambahan_judul[1]=$pertambahan_judul[1]+1;
		if($add_data['source']==2) $pertambahan_judul[2]=$pertambahan_judul[2]+1;
		if($add_data['source']==3) $pertambahan_judul[3]=$pertambahan_judul[3]+1;
		if($add_data['source']==4) $pertambahan_judul[4]=$pertambahan_judul[4]+1;
	}
	$total_pertambahan_judul_perclass=0;
	$total_pertambahan_judul_perclass=$pertambahan_judul[1]+$pertambahan_judul[2]+$pertambahan_judul[3]+$pertambahan_judul[4];
	$grand_total_pertambahan_judul=$grand_total_pertambahan_judul+$total_pertambahan_judul_perclass;
	
	$total_pertambahan_judul[1]=$total_pertambahan_judul[1]+$pertambahan_judul[1];
	$total_pertambahan_judul[2]=$total_pertambahan_judul[2]+$pertambahan_judul[2];
	$total_pertambahan_judul[3]=$total_pertambahan_judul[3]+$pertambahan_judul[3];
	$total_pertambahan_judul[4]=$total_pertambahan_judul[4]+$pertambahan_judul[4];

	$grand_total_judul_perklass=0;
	$grand_total_judul_perklass=$total_judul_perklass+$total_pertambahan_judul_perclass;
	$jml_grand_total_judul_perklass=$jml_grand_total_judul_perklass+$grand_total_judul_perklass;
	
	//count tambahan eksemplar/koleksi buku per klasifikasi per gmd_id;
	$sql="select count(t1.biblio_id) as jumlah ,t1.source from item as t1,biblio as t2 where t2.classification_no like '".$class_data['class_number']."%' and month(t1.received_date)='".$_GET['bulan']."' and year(t1.received_date)='".$_GET['tahun']."' and t1.biblio_id=t2.biblio_id group by t1.source order by t1.source asc";
	$add_query=$dbs->query($sql);
	$pertambahan_eks[1]=$pertambahan_eks[2]=$pertambahan_eks[3]=$pertambahan_eks[4]=0;
	while($add_eks=$add_query->fetch_array()){
		if($add_eks['source']==1) $pertambahan_eks[1]=$add_eks['jumlah'];
		if($add_eks['source']==2) $pertambahan_eks[2]=$add_eks['jumlah'];
		if($add_eks['source']==3) $pertambahan_eks[3]=$add_eks['jumlah'];
		if($add_eks['source']==4) $pertambahan_eks[4]=$add_eks['jumlah'];
	}

	$total_pertambahan_eks_perklass=0;
	$total_pertambahan_eks_perklass=$pertambahan_eks[1]+$pertambahan_eks[2]+$pertambahan_eks[3]+$pertambahan_eks[4];
	$grand_total_pertambahan_eks=$grand_total_pertambahan_eks+$total_pertambahan_eks_perclass;

	$total_pertambahan_eks[1]=$total_pertambahan_eks[1]+$pertambahan_eks[1];
	$total_pertambahan_eks[2]=$total_pertambahan_eks[2]+$pertambahan_eks[2];
	$total_pertambahan_eks[3]=$total_pertambahan_eks[3]+$pertambahan_eks[3];
	$total_pertambahan_eks[4]=$total_pertambahan_eks[4]+$pertambahan_eks[4];

    $grand_total_eks_perklass=0;
	$grand_total_eks_perklass=$total_eks_perklass+$total_pertambahan_eks_perklass;
	$jml_grand_total_eks_perklass=$jml_grand_total_eks_perklass+$grand_total_eks_perklass;


	$final_judul_perklass=$grand_total_judul_perklass - $grand_total_judul_rusak;
	$final_eks_perklass=$grand_total_eks_perklass- $grand_total_eks_rusak;
	
	$grand_final_total_judul=$grand_final_total_judul+ $final_judul_perklass;
	$grand_final_total_eks=$grand_final_total_eks+ $final_eks_perklass;


	$html_str .= '  <tr class"body">';
	$html_str .= '	<td>'.$no++.'</td>';
	$html_str .= '	<td style="text-align:left; padding-left:2px;">'.$class_data['class_number'].' '.$class_data['class_name'].'</td>';
	$html_str .= '	<td>'.$data_jumlah_judul[1].'</td>';
	$html_str .= '	<td>'.$data_jumlah_judul[2].'</td>';
	$html_str .= '	<td>'.$data_jumlah_judul[3].'</td>';
	$html_str .= '	<td>'.$data_jumlah_judul[4].'</td>';
	$html_str .= '	<td>'.$data_jumlah_eks[1].'</td>';
	$html_str .= '	<td>'.$data_jumlah_eks[2].'</td>';
	$html_str .= '	<td>'.$data_jumlah_eks[3].'</td>';
	$html_str .= '	<td>'.$data_jumlah_eks[4].'</td>';
	$html_str .= '	<td>'.$pertambahan_judul[1].'</td>';
	$html_str .= '	<td>'.$pertambahan_judul[2].'</td>';
	$html_str .= '	<td>'.$pertambahan_judul[3].'</td>';
	$html_str .= '	<td>'.$pertambahan_judul[4].'</td>';
	$html_str .= '	<td>'.$pertambahan_eks[1].'</td>';
	$html_str .= '	<td>'.$pertambahan_eks[2].'</td>';
	$html_str .= '	<td>'.$pertambahan_eks[3].'</td>';
	$html_str .= '	<td>'.$pertambahan_eks[4].'</td>';
	$html_str .= '	<td>'.$grand_total_judul_perklass.'</td>';
	$html_str .= '	<td>'.$grand_total_eks_perklass.'</td>';
	$html_str .= '	<td>'.$data_jumlah_rusak_judul[1].'</td>';
	$html_str .= '	<td>'.$data_jumlah_rusak_judul[2].'</td>';
	$html_str .= '	<td>'.$data_jumlah_rusak_judul[3].'</td>';
	$html_str .= '	<td>'.$data_jumlah_rusak_judul[4].'</td>';
	$html_str .= '	<td>'.$data_jumlah_rusak_eks[1].'</td>';
	$html_str .= '	<td>'.$data_jumlah_rusak_eks[2].'</td>';
	$html_str .= '	<td>'.$data_jumlah_rusak_eks[3].'</td>';
	$html_str .= '	<td>'.$data_jumlah_rusak_eks[4].'</td>';
	$html_str .= '	<td>'.$final_judul_perklass.'</td>';
	$html_str .= '	<td>'.$final_eks_perklass.'</td>';
	$html_str .= '  </tr>';
	
	}
		
	$html_str .= '  <tr class="total">';
	$html_str .= '	<td>&nbsp;</td>';
	$html_str .= '	<td style="text-align:left; padding-left:2px;">J U M L A H </td>';
	$html_str .= '	<td>'.$total_judul[1].'</td>';
	$html_str .= '	<td>'.$total_judul[2].'</td>';
	$html_str .= '	<td>'.$total_judul[3].'</td>';
	$html_str .= '	<td>'.$total_judul[4].'</td>';
	$html_str .= '	<td>'.$total_eks[1].'</td>';
	$html_str .= '	<td>'.$total_eks[2].'</td>';
	$html_str .= '	<td>'.$total_eks[3].'</td>';
	$html_str .= '	<td>'.$total_eks[4].'</td>';
	$html_str .= '	<td>'.$total_pertambahan_judul[1].'</td>';
	$html_str .= '	<td>'.$total_pertambahan_judul[2].'</td>';
	$html_str .= '	<td>'.$total_pertambahan_judul[3].'</td>';
	$html_str .= '	<td>'.$total_pertambahan_judul[4].'</td>';
	$html_str .= '	<td>'.$total_pertambahan_eks[1].'</td>';
	$html_str .= '	<td>'.$total_pertambahan_eks[2].'</td>';
	$html_str .= '	<td>'.$total_pertambahan_eks[3].'</td>';
	$html_str .= '	<td>'.$total_pertambahan_eks[4].'</td>';
	$html_str .= '	<td>'.$jml_grand_total_judul_perklass.'</td>';
	$html_str .= '	<td>'.$jml_grand_total_eks_perklass.'</td>';
	$html_str .= '	<td>'.$total_judul_rusak[1].'</td>';
	$html_str .= '	<td>'.$total_judul_rusak[2].'</td>';
	$html_str .= '	<td>'.$total_judul_rusak[3].'</td>';
	$html_str .= '	<td>'.$total_judul_rusak[4].'</td>';
	$html_str .= '	<td>'.$total_eks_rusak[1].'</td>';
	$html_str .= '	<td>'.$total_eks_rusak[2].'</td>';
	$html_str .= '	<td>'.$total_eks_rusak[3].'</td>';
	$html_str .= '	<td>'.$total_eks_rusak[4].'</td>';
	$html_str .= '	<td>'.$grand_final_total_judul.'</td>';
	$html_str .= '	<td>'.$grand_final_total_eks.'</td>';

	$html_str .= '  </tr>';
		
	$html_str .= '</table>';
	$html_str .= '<br>';
	$html_str .= '<u>Keterangan :</u><br>';
	$html_str .= '1= Paket<br>';
	$html_str .= '2= Pembelian<br>';
	$html_str .= '3= Hadiah<br>';
	$html_str .= '4= Pelajaran<br>';
    $html_str .= '<script type="text/javascript">self.print();</script>'."\n";
	$html_str .= '</body>';
	$html_str .= '</html>';

    // write to file
    $file_write = @file_put_contents(REPBS.'keadaan_koleksi_print_result.html', $html_str);
    if ($file_write) {
        // open result in new window
        //echo "test";
		echo '<script type="text/javascript">parent.openWin(\''.SWB.FLS.'/'.REP.'/keadaan_koleksi_print_result.html\', \'popMemberReport\', 1000, 500, true)</script>';
    } else { utility::jsAlert('ERROR! Loan statistic report failed to generate, possibly because '.REPBS.' directory is not writable'); }
    exit();
}

?>
    <fieldset>
    <div class="per_title">
      <h2><?php echo __('Keadaan Koleksi Berdasar Kelas'); ?></h2>
    </div>
    <div class="sub_section">
    <form name="printForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" target="submitPrint" id="printForm" method="get" style="display: inline;">
    <div id="filterForm">
        <div class="divRow">
            <div class="divRowContent">
                <select name="bulan">
                    <option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>    
                </select>
                <select name="tahun">
                <?php for($i = 2009; $i <= date('Y') ; $i++) { ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php } ?>  
                </select>
                <input type="submit" value="<?php echo "Cetak Laporan"; ?>" class="button" />
            </div>
        </div>

    </div>
    <input type="hidden" name="print" value="true" />
    </form>
    </div>
    </fieldset>
    <iframe name="submitPrint" style="visibility: hidden; width: 0; height: 0;"></iframe>