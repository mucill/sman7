<?php

/*

VERSION : 3.0
CODENAME : SENAYAN
AUTHOR :
    Code and Programming : ARIE NUGRAHA (dicarve@yahoo.com)
    Database Design : HENDRO WICAKSONO (hendrowicaksono@yahoo.com) & WARDIYONO (wynerst@gmail.com)
	Customize : TOTO PRIYONO (toto_priyono@yahoo.com )

SENAYAN Library Automation System
Copyright (C) 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (GPL License.txt); if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* Reporting section */

// start the session
session_start();

require '../../../sysconfig.inc.php';
require SIMBIO_BASE_DIR.'simbio_GUI/table/simbio_table.inc.php';
$bulan =array(
		'1'=>'Januari',
		'2'=>'Februari',
		'3'=>'Maret',
		'4'=>'April',
		'5'=>'Mei',
		'6'=>'Juni',
		'7'=>'Juli',
		'8'=>'Agustus',
		'9'=>'September',
		'10'=>'Oktober',
		'11'=>'November',
		'12'=>'Desember'
		);
		
		
$label_source=array('0'=>'Semua Sumber','1'=>'Paket','2'=>'Pembelian','3'=>'Hadiah','4'=>'Pelajaran');		
// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.lang_sys_common_no_privilage.'</div>');
}


// total number of titles

// if we are in print mode
if (isset($_GET['print'])) {
    // html strings
	
	$date1=$_GET['tahun1'].'-'.$_GET['bulan1'].'-'.$_GET['tanggal1'];
	$date2=$_GET['tahun2'].'-'.$_GET['bulan2'].'-'.$_GET['tanggal2'];
	
	
	//statistik pengunjung berdasarkan inst_name/kelas
	$sql="select m.inst_name,t.member_type_name, count(m.member_id) as frek from presensi as p, member as m, mst_member_type as t where p.member_id=m.member_id and m.member_type_id=t.member_type_id and p.tanggal>='".$date1."' and p.tanggal<='".$date2."' ";
	if($_GET['member_type']>0){
	  $sql .=" and  m.member_type_id=".$_GET['member_type'];
	}
	$sql .= " group by m.inst_name order by frek desc";

	$hasil_query = $dbs->query($sql);

	$html_str = '<html><head><title>Frekuensi Peminjam</title>
				<style type="text/css">
					table.table_koleksi {font-size:13px;border-bottom:1px solid #000;border-top:1px solid #000;}
					.table_koleksi td{border-bottom:1px solid #000;border-top:1px solid #000; text-align:left;}		
					.table_koleksi td.judul{text-align:left;}		
					.table_koleksi td.rata-kanan{text-align:center;}		
					.table_koleksi th{background-color:#999999;}
					.title_report{ font-size:15px; text-align:center; font-weight:bold;}
				</style>
				</head><body>
				<div class="title_report"><p>Sekolah Menengah Atas Negeri 7 Purworejo <br>Jl. Ki Mangunsarkoro No. 1 Purworejo <br><br>
				Statistik Pengunjung Berdasarkan Kelas/Jenis Anggota <br>Periode : '.$_GET['tanggal1'].'/'.$_GET['bulan1'].'/'.$_GET['tahun1'].' s.d '.$_GET['tanggal2'].'/'.$_GET['bulan2'].'/'.$_GET['tahun2'].' <br><br>Tanggal Cetak :'.date('l , d/m/Y H:i:s').'WIB </p></div>
  				
				<table width="70%" align="center" cellpadding="0" cellspacing="0" class="table_koleksi">
    				<tr><th width="5%">No</th><th width="50%">Kelas/Jenis Anggota </th><th width="15%">Kategori Anggota</th><th width="20%">Jumlah</th></tr>';  				
		$no=0;
		$total=0;
		while($data=$hasil_query->fetch_array()){
			$html_str .=   '<tr>
								<td>'.++$no.'</td>
								<td>'.$data['inst_name'].'</td>
								<td>'.$data['member_type_name'].'</td>
								<td class="rata-kanan">'.$data['frek'].'</td>
							</tr>';
			$total=$total+$data['frek'];											
		}
			$html_str .=   '<tr>
								<td colspan="3">T O T A L</td>
								<td class="rata-kanan">'.$total.'</td>
							</tr>';
 
   
    $html_str .= '</table><br><br>';
	
	$sql="select t.member_type_name, count(m.member_id) as frek from presensi as p, member as m, mst_member_type as t where p.member_id=m.member_id and m.member_type_id=t.member_type_id and p.tanggal>='".$date1."' and p.tanggal<='".$date2."' ";
	$sql .= " group by m.member_type_id order by frek desc";
	$hasil_query = $dbs->query($sql);

	$html_str .= '<div class="title_report">Statistik Pengunjung Berdasarkan Kategori Anggota <br>Periode : '.$_GET['tanggal1'].'/'.$_GET['bulan1'].'/'.$_GET['tahun1'].' s.d '.$_GET['tanggal2'].'/'.$_GET['bulan2'].'/'.$_GET['tahun2'].' <br><br></div>
  				
				<table width="70%" align="center" cellpadding="0" cellspacing="0" class="table_koleksi">
    				<tr><th width="5%">No</th><th width="15%">Kategori Anggota</th><th width="20%">Jumlah</th></tr>';  				
		$no=0;
		$total=0;
		while($data=$hasil_query->fetch_array()){	
			$html_str .=   '<tr>
								<td>'.++$no.'</td>
								<td>'.$data['member_type_name'].'</td>
								<td class="rata-kanan">'.$data['frek'].'</td>
							</tr>';
							$total=$total+$data['frek'];
		}				
 
	$html_str .=   '<tr>
						<td colspan="2">T O T A L</td>
						<td class="rata-kanan">'.$total.'</td>
					</tr>';

    $html_str .= '</table><br><br>';
	
	
	$sql="select m.member_id, m.member_name,t.member_type_name, m.inst_name, count(m.member_id) as frek from presensi as p, member as m, mst_member_type as t where p.member_id=m.member_id and m.member_type_id=t.member_type_id and p.tanggal>='".$date1."' and p.tanggal<='".$date2."' ";
	if($_GET['member_type']>0){
	  $sql .=" and  m.member_type_id=".$_GET['member_type'];
	}	
	$sql.=" group by m.member_id order by frek desc limit 10";
	$hasil_query = $dbs->query($sql);

	$html_str .= '<div class="title_report">Daftar 10 Pengunjung Paling Rajin <br>Periode : '.$_GET['tanggal1'].'/'.$_GET['bulan1'].'/'.$_GET['tahun1'].' s.d '.$_GET['tanggal2'].'/'.$_GET['bulan2'].'/'.$_GET['tahun2'].' <br><br></div>
  				
				<table width="70%" align="center" cellpadding="0" cellspacing="0" class="table_koleksi">
    				<tr><th width="5%">No</th><th width="45%">Nama Anggota</th><th width="15%">Kategori</th><th width="15%">Kelas/Institusi</th><th width="10%">Jumlah</th></tr>';  				
		$no=0;
		while($data=$hasil_query->fetch_array()){	
			$html_str .=   '<tr>
								<td>'.++$no.'</td>
								<td>'.$data['member_name'].' ('.$data['member_id'].')</td>
								<td>'.$data['member_type_name'].'</td>
								<td>'.$data['inst_name'].'</td>
								<td class="rata-kanan">'.$data['frek'].'</td>
							</tr>';
		}				
 
    $html_str .= '</table><br><br>';
	

	
	
	
    $html_str .= '<script type="text/javascript">self.print();</script>'."\n";
	$html_str .= '</body></html>';

	
    // write to file
    $file_write = @file_put_contents(REPORT_FILE_BASE_DIR.'daftar_koleksi_print_result.html', $html_str);
    if ($file_write) {
        // open result in new window
        //echo "test";
		echo '<script type="text/javascript">parent.openWin(\'http://'.$_SERVER['HTTP_HOST'].'/'.FILES_DIR.'/'.REPORT_DIR.'/daftar_koleksi_print_result.html\', \'popMemberReport\', 1000, 500, true)</script>';
    } else { utility::jsAlert('ERROR! Loan statistic report failed to generate, possibly because '.REPORT_FILE_BASE_DIR.' directory is not writable'); }
    exit();
}

?>
<table id="searchForm" cellpadding="5" cellspacing="0">
<tr>
    <td class="imageLeft" valign="top" style="background-image: url(<?php echo $sysconf['admin_template']['dir'].'/'.$sysconf['admin_template']['theme'].'/statistic.png'; ?>)">
        <?php echo strtoupper('Statistik Pengunjung'); ?>
                <hr />
        <form name="printForm" action="<?php echo $_SERVER['PHP_SELF']; ?>"   target="submitPrint" id="printForm" method="get" style="display: inline;">
		<table>
			<tr>
				<td>
		Periode Kunjungan 
				</td>
				<td>
		<select name="tanggal1">        
			<?
			  for($i=1;$i<=31;$i++){
			?>
			<option value="<?=$i?>"><?=$i?></option>
			<?
			}
			?>
		</select>
		<select name="bulan1">
			<option value="1">Januari</option>
			<option value="2">Februari</option>
			<option value="3">Maret</option>
			<option value="4">April</option>
			<option value="5">Mei</option>
			<option value="6">Juni</option>
			<option value="7">Juli</option>
			<option value="8">Agustus</option>
			<option value="9">September</option>
			<option value="10">Oktober</option>
			<option value="11">November</option>
			<option value="12">Desember</option>	
		</select>
		<select name="tahun1">
		<? for($i=1991;$i<=(date('Y')+1);$i++) {?>
			<option value="<?=$i?>"><?=$i?></option>
		<? }?>	
		</select>
		 s.d 
		 
		<select name="tanggal2">        
			<?
			  for($i=1;$i<=31;$i++){
			?>
			<option value="<?=$i?>"><?=$i?></option>
			<?
			}
			?>
		</select>
		<select name="bulan2">
			<option value="1">Januari</option>
			<option value="2">Februari</option>
			<option value="3">Maret</option>
			<option value="4">April</option>
			<option value="5">Mei</option>
			<option value="6">Juni</option>
			<option value="7">Juli</option>
			<option value="8">Agustus</option>
			<option value="9">September</option>
			<option value="10">Oktober</option>
			<option value="11">November</option>
			<option value="12">Desember</option>	
		</select>
		<select name="tahun2">
		<? for($i=1991;$i<=(date('Y')+1);$i++) {?>
			<option value="<?=$i?>"><?=$i?></option>
		<? }?>	
		</select>
				</td>
			</tr>
			<tr>
				<td>
					Kategori Anggota 
				</td>
				<td>
				<?
					$sql="select * from mst_member_type order by member_type_id asc";
					$hasil_query=$dbs->query($sql);
					
				?>
		<select name="member_type">
			<option value="0">All</option>
			<?
				while($data=$hasil_query->fetch_array()){
			?>
			<option value="<?=$data['member_type_id']?>"><?=$data['member_type_name']?></option>
			<?
			}
			?>
		
		</select>
				</td>
			</tr>
		    <tr>
				<td colspan="2">
		<input type="hidden" name="print" value="true" /><input type="submit" value="<?php echo "Cetak Laporan"; ?>" class="button" />
                </form>
                <iframe name="submitPrint" style="visibility: hidden; width: 0; height: 0;"></iframe>
    			</td>
			</tr>	
		</table>

	</td>
</tr>
</table>
<?php
/* collection statistic end */
?>
