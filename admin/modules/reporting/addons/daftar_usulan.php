<?php
/**
 * Copyright (C) 2012 Arie Nugraha (dicarve@yahoo.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/* Z3950 Web Services section */

// key to authenticate
define('INDEX_AUTH', '1');

// key to get full database access
define('DB_ACCESS', 'fa');

if (!isset ($errors)) {
    $errors = false;
}

// start the session
require '../../../../sysconfig.inc.php';

// IP based access limitation
require LIB.'ip_based_access.inc.php';

do_checkIP('smc');
do_checkIP('smc-bibliography');

require SB.'admin/default/session.inc.php';
require SB.'admin/default/session_check.inc.php';
require SIMBIO.'simbio_GUI/table/simbio_table.inc.php';
require SIMBIO.'simbio_GUI/paging/simbio_paging.inc.php';
require SIMBIO.'simbio_DB/simbio_dbop.inc.php';

// privileges checking
$can_read = utility::havePrivilege('bibliography', 'r');
$can_write = utility::havePrivilege('bibliography', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.__('You are not authorized to view this section').'</div>');
}

$page_title     = 'Daftar Usulan Koleksi Sudah Diproses';
$reportView     = false;
$num_recs_show  = 20;
if (isset($_GET['reportView'])) {
    $reportView = true;
}

if(isset($_POST['id'])) {
    foreach ($_POST['id'] as $key => $id) {
        $sql = 'UPDATE usulan_buku SET status = 1 WHERE id = '.$id;
        $dbs->query($sql);
        utility::jsAlert('Usulan berhasil di proses');
        echo '<script type="text/javascript">parent.$(\'#mainContent\').simbioAJAX(\''.$_SERVER['PHP_SELF'].'\');</script>';
    }
}

if (!$reportView) {
?>
<fieldset class="menuBox">
    <div class="menuBoxInner memberIcon">
        <div class="per_title">
            <h2><?php echo __('Daftar Usulan Buku'); ?></h2>
        </div>
    </div>
</fieldset>
<iframe name="reportView" id="reportView" src="<?php echo $_SERVER['PHP_SELF'].'?reportView=true'; ?>" frameborder="0" style="width: 100%; height: 500px;"></iframe>
<?php
} else {
    ob_start();
    echo '<form method="post" action="'.$_SERVER['PHP_SELF'].'">';
    echo '<table id="dataList" cellpadding="5" cellspacing="0">';
    echo '<thead>';
    echo '<tr class="dataListHeader">';
    echo '<td>Proses</td>';
    echo '<td>Tanggal</td>';
    echo '<td>Nama Pengusul</td>';
    echo '<td>Judul</td>';
    echo '<td>Pengarang</td>';
    echo '<td>Penerbit</td>';
    echo '<td>Alasan</td>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    // get usulan buku yang belum diproses
    $sql = "SELECT id, DATE_FORMAT(tanggal, '%d.%m.%Y %h:%i:%s') tanggal,nama,judul,pengarang,penerbit,alasan FROM usulan_buku WHERE status = 0";
    $query = $dbs->query($sql);
    // echo '<pre>'.print_r($dbs,1).'</pre>';
    if($query->num_rows > 0 ){
        while($usulan = $query->fetch_object()) {
            echo '<tr>';
            echo '<td><input type="checkbox" name="id[]" value="'.$usulan->id.'"></td>';
            echo '<td nowrap>'.$usulan->tanggal.'</td>';
            echo '<td>'.$usulan->nama.'</td>';
            echo '<td>'.$usulan->judul.'</td>';
            echo '<td>'.$usulan->pengarang.'</td>';
            echo '<td>'.$usulan->penerbit.'</td>';
            echo '<td>'.$usulan->alasan.'</td>';
            echo '</tr>';
        }
    }
    echo '</tbody>';
    echo '</table>';
    echo '<div style="padding-top: 15px; clear: both;">
          <input type="submit" name="applyFilter" value="'.__('Proses Usulan').'" />
          <input type="hidden" name="reportView" value="true" />
          </div>';
    echo '</form>';
    $content = ob_get_clean();
    // include the page template
    require SB.'/admin/'.$sysconf['admin_template']['dir'].'/printed_page_tpl.php';
}