<?php 

/*

VERSION : 3.0
CODENAME : SENAYAN
AUTHOR :
    Code and Programming : ARIE NUGRAHA (dicarve@yahoo.com)
    Database Design : HENDRO WICAKSONO (hendrowicaksono@yahoo.com) & WARDIYONO (wynerst@gmail.com)
    Customize : TOTO PRIYONO (toto_priyono@yahoo.com ) & Eddy Subratha (eddy.subratha@slims.web.id)

SENAYAN Library Automation System
Copyright (C) 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (GPL License.txt); if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* Reporting section */

// key to authenticate
define('INDEX_AUTH', '1');

// main system configuration
require '../../../../sysconfig.inc.php';

// IP based access limitation
require LIB.'ip_based_access.inc.php';
do_checkIP('smc');
do_checkIP('smc-reporting');

// start the session
require SB.'admin/default/session.inc.php';
require SB.'admin/default/session_check.inc.php';

// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.__('You don\'t have enough privileges to access this area!').'</div>');
}

$bulan =array(
        '1'=>'Januari',
        '2'=>'Februari',
        '3'=>'Maret',
        '4'=>'April',
        '5'=>'Mei',
        '6'=>'Juni',
        '7'=>'Juli',
        '8'=>'Agustus',
        '9'=>'September',
        '10'=>'Oktober',
        '11'=>'November',
        '12'=>'Desember'
        );

$label_source=array('0'=>'Semua Sumber','1'=>'Paket','2'=>'Pembelian','3'=>'Hadiah','4'=>'Pelajaran');      
// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.lang_sys_common_no_privilage.'</div>');
}


// total number of titles

// if we are in print mode
if (isset($_GET['print'])) {
    // html strings
    
    $date1=$_GET['tahun1'].'-'.$_GET['bulan1'].'-'.$_GET['tanggal1'];
    $date2=$_GET['tahun2'].'-'.$_GET['bulan2'].'-'.$_GET['tanggal2'];
    
    $sql="select l.member_id , m.member_name, m.inst_name, t.member_type_name, count(l.member_id) as jumlah from  loan as l , member as m, mst_member_type as t  where l.loan_date>='".$date1."' and l.loan_date<='".$date2."' ";   
    if($_GET['member_type']>0){
     $sql .=" and  m.member_type_id=".$_GET['member_type'];
    }
    $sql .= " and l.member_id=m.member_id and m.member_type_id=t.member_type_id group by l.member_id order by jumlah desc";

    //echo $sql;
    
    $hasil_query = $dbs->query($sql);


    $html_str = '<html><head><title>Frekuensi Peminjam</title>
                <style type="text/css">
                    table.table_koleksi {font-size:13px;border-bottom:1px solid #000;border-top:1px solid #000;}
                    .table_koleksi td{border-bottom:1px solid #000;border-top:1px solid #000; text-align:left;}     
                    .table_koleksi td.judul{text-align:left;}       
                    .table_koleksi td.rata-kanan{text-align:center;}        
                    .table_koleksi th{background-color:#999999;}
                    .title_report{ font-size:15px; text-align:center; font-weight:bold;}
                </style>
                </head><body>
                <div class="title_report"><p>Sekolah Menengah Atas Negeri 7 Purworejo <br>Jl. Ki Mangunsarkoro No. 1 Purworejo <br><br>
                Daftar Koleksi <br>Periode Peminjaman: '.$_GET['tanggal1'].'/'.$_GET['bulan1'].'/'.$_GET['tahun1'].' s.d '.$_GET['tanggal2'].'/'.$_GET['bulan2'].'/'.$_GET['tahun2'].' <br><br>Tanggal Cetak :'.date('l , d/m/Y H:i:s').'WIB </p></div>
                
                <table width="70%" align="center" cellpadding="0" cellspacing="0" class="table_koleksi">
                    <tr><th width="5%">No</th><th width="50%">Nama Anggota </th><th width="15%">Kategori Anggota </th><th width="20%">Kelas/Instansi </th><th width="5%">Frekuensi Pinjam</th></tr>';               

        $total=0;
        while($data=$hasil_query->fetch_array()){
    
            $html_str .=   '<tr>
                                <td>'.++$no.'</td>
                                <td>'.$data['member_name'].' ('.$data['member_id'].')</td>
                                <td>'.$data['member_type_name'].'</td>
                                <td>'.$data['inst_name'].'</td>
                                <td class="rata-kanan">'.$data['jumlah'].'</td>
                            </tr>';
                            $total=$total+$data['jumlah'];
        }               
            $html_str .=   '<tr>
                                <td colspan="4">T O T A L</td>
                                <td class="rata-kanan">'.$total.'</td>
                            </tr>';
 
 
    $html_str .= '</table><br><br>';
    $html_str .= '<script type="text/javascript">self.print();</script>'."\n";
    $html_str .= '</body></html>';

    
    // write to file
    $file_write = @file_put_contents(REPBS.'daftar_koleksi_print_result.html', $html_str);
    if ($file_write) {
        // open result in new window
        //echo "test";
        echo '<script type="text/javascript">parent.openWin(\''.SWB.FLS.'/'.REP.'/daftar_koleksi_print_result.html\', \'popMemberReport\', 1000, 500, true)</script>';
    } else { utility::jsAlert('ERROR! Loan statistic report failed to generate, possibly because '.REPBS.' directory is not writable'); }
    exit();
}
?>

<fieldset>
    <div class="per_title">
      <h2><?php echo __('Frekuensi Peminjaman'); ?></h2>
    </div>
    <div class="sub_section">
        <form name="printForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" target="submitPrint" id="printForm" method="get" style="display: inline;">
            <div id="filterForm">
                <div class="divRow">
                    <div class="divRowContent">
                        <div style="width: 175px; text-align: right; padding: 10px 20px 0 0; float: left;">Periode Peminjaman</div>
                        <select name="tanggal1">        
                            <?php foreach(range(1,31) as $i) { ?>
                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php $i++; } ?>
                        </select>
                        <select name="bulan1">
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>    
                        </select>
                        <select name="tahun1">
                        <?php foreach(range(1991, date('Y')) as $i) { ?>
                            <option value="<?php echo  $i ?>"><?php echo  $i?></option>
                        <?php $i++; } ?>  
                        </select>                    
                    </div>
                    <div class="divRowContent">
                    <div style="width: 175px; text-align: right; padding: 10px 20px 0 0; float: left;">Sampai dengan</div>
                    <select name="tanggal2">        
                        <?php foreach(range(1,31) as $i) { ?>
                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                        <?php $i++; } ?>
                    </select>
                    <select name="bulan2">
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>    
                    </select>
                    <select name="tahun2">
                    <?php foreach(range(1991, date('Y')) as $i) { ?>
                        <option value="<?php echo  $i ?>"><?php echo  $i?></option>
                    <?php $i++; } ?>  
                    </select>
                    </div>
                    <div class="divRowContent">
                        <div style="width: 175px; text-align: right; padding: 10px 20px 0 0; float: left;">Kategori Anggota</div>
                        <?php
                        $sql="SELECT * FROM mst_member_type ORDER BY member_type_id ASC";
                        $hasil_query=$dbs->query($sql);
                        ?>
                        <select name="member_type">
                            <option value="0">All</option>
                            <?php while($data = $hasil_query->fetch_array()) { ?>
                            <option value="<?php echo $data['member_type_id'] ?>"><?php echo $data['member_type_name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="width: 175px; text-align: right; padding: 10px 20px 0 0; float: left;"></div>
                     <input type="hidden" name="print" value="true" /><input type="submit" value="<?php echo "Cetak Laporan"; ?>" class="button" />
                </div>
            </div>
        </form>
    </div>
</fieldset>
<iframe name="submitPrint" style="visibility: hidden; width: 0; height: 0;"></iframe>   
