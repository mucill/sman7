<?php
// start the session
session_start();

require '../../../sysconfig.inc.php';
require SIMBIO_BASE_DIR.'simbio_GUI/table/simbio_table.inc.php';

//print_r($_POST);
// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.lang_sys_common_no_privilage.'</div>');
}

$str="select * from mst_member_type order by member_type_id asc";
$query=$dbs->query($str);
while($data=$query->fetch_array()){
	$jenis_member[$data['member_type_id']]['id']=$data['member_type_id'];
	$jenis_member[$data['member_type_id']]['name']=$data['member_type_name'];
}

$bulan=array('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des');
$tahun=$_GET['tahun'];
//$str="select t2.member_type_id as member_type_id,t2.member_type_name as jenis_member,count(t1.member_id) as total, month(t1.loan_date) as bulan from loan as t1,mst_member_type as t2 , member as t3 where t1.member_id=t3.member_id and t2.member_type_id=t3.member_type_id and year(loan_date)= $tahun group by jenis_member,bulan  order by bulan,jenis_member asc";
$str="select t2.member_type_id as member_type_id,t2.member_type_name as jenis_member,count(t1.presensi_id) as total, month(t1.tanggal) as bulan from presensi as t1,mst_member_type as t2 , member as t3 where t1.member_id=t3.member_id and t2.member_type_id=t3.member_type_id and year(t1.tanggal)= $tahun group by member_type_id,bulan  order by bulan,member_type_id asc";

$query=$dbs->query($str);
while($data=$query->fetch_array()){
	foreach ($jenis_member as $item){
		if($data['member_type_id']==$item['id']) {
			$data_pengunjung[$item['id']][$data['bulan']]=$data['total']; 
			$data_pengunjung[$item['id']]['id']=$item['id'];
		}
	}
}

?>

<style type="text/css">
.report_box{
text-align:center;
margin:0 auto;

}
.report_box table{
border-left:1px solid #000;
border-top:1px solid #000;
}

.report_box td{
text-align:left;
border-bottom:1px solid #000;
border-right:1px solid #000;
padding-left:2px;
}

.report_box td.data{
text-align:right;
padding-right:4px;
}

h2{
color:#FFFFFF;
background-color:#000066;
height:30px;
font-size:16px;
padding-top:5px;

}

h2{
color:#FFFFFF;
background-color:#000066;
height:30px;
font-size:16px;
padding-top:5px;

}

</style>


<div class="report_box">
<h2>Jumlah Pengunjung Tahun <?=$tahun?></h2>
<br>
<table width="98%" cellpadding="0" cellspacing="0">
  <tr bgcolor="#eee">
    <td><strong>Jenis Member </strong></td>
	<? for($i=1;$i<=12;$i++){ ?>
		<td><strong>
		<?=$bulan[$i-1]?>
		</strong></td>
  	<?
	}
	?>
  </tr>
  <? 
  	$i=0;
	foreach($data_pengunjung as $item){ 
	?>
  <tr>
    <td><?=$jenis_member[$item['id']]['name']?></td>
    <td class="data"><?=($item[1]=='')?0:$item[1]?></td>
    <td class="data"><?=($item[2]=='')?0:$item[2]?></td>
    <td class="data"><?=($item[3]=='')?0:$item[3]?></td>
    <td class="data"><?=($item[4]=='')?0:$item[4]?></td>
    <td class="data"><?=($item[5]=='')?0:$item[5]?></td>
    <td class="data"><?=($item[6]=='')?0:$item[6]?></td>
    <td class="data"><?=($item[7]=='')?0:$item[7]?></td>
    <td class="data"><?=($item[8]=='')?0:$item[8]?></td>
    <td class="data"><?=($item[9]=='')?0:$item[9]?></td>
    <td class="data"><?=($item[10]=='')?0:$item[10]?></td>
    <td class="data"><?=($item[11]=='')?0:$item[11]?></td>
    <td class="data"><?=($item[12]=='')?0:$item[12]?></td>
  </tr>
  <? 
  	$i++;
  } 
  ?>
</table>
</div>
