<?php

// start the session
session_start();

require '../../../sysconfig.inc.php';
require SIMBIO_BASE_DIR.'simbio_GUI/table/simbio_table.inc.php';

//print_r($_POST);
// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.lang_sys_common_no_privilage.'</div>');
}

$str="select * from mst_member_type";
$query=$dbs->query($str);
$i=0;
while($data=$query->fetch_array()){
	$jenis_member[$i]['id']=$data['member_type_id'];
	$jenis_member[$i]['name']=$data['member_type_name'];
	$member_type[$data['member_type_id']]=$data['member_type_name'];
	$i++;
}

$tahun_sekarang=date('Y');
$start_tahun=$tahun_sekarang-5;
$str="select t2.member_type_id as member_type_id,t2.member_type_name as jenis_member,count(t1.presensi_id) as total, year(t1.tanggal) as tahun from presensi as t1,mst_member_type as t2 , member as t3 where t1.member_id=t3.member_id and t2.member_type_id=t3.member_type_id and year(t1.tanggal)>=$start_tahun and year(t1.tanggal)<=$tahun_sekarang group by member_type_id,tahun  order by tahun,member_type_id asc";
$query=$dbs->query($str);
while($data=$query->fetch_array()){
	foreach($jenis_member as $item){
		if ($data['member_type_id']==$item['id']) { 
			$statistik_pengunjung[$data['member_type_id']]['id']=$data['member_type_id'];
			$statistik_pengunjung[$data['member_type_id']][$data['tahun']]=$data['total'];
		}
	}
}

?>

<style type="text/css">
.report_box{
text-align:center;
margin:0 auto;

}
.report_box table{
border-left:1px solid #000;
border-top:1px solid #000;
}

.report_box td{
text-align:left;
border-bottom:1px solid #000;
border-right:1px solid #000;
padding-left:2px;

}

.report_box td.data{
text-align:right;
padding-right:4px;

}

h2{
color:#FFFFFF;
background-color:#000066;
height:30px;
font-size:16px;
padding-top:5px;

}

</style>


<div class="report_box">
<h2>Statistik Jumlah Pengunjung Periode 5 Tahun Terakhir</h2>
<br>
<table width="80%" cellpadding="0" cellspacing="0">
  <tr bgcolor="#eee">
    <td><strong>Jenis Member </strong></td>
	<? for($i=5;$i>=0;$i--){ ?>
		<td><strong>
		<a onclick="setContent('mainContent', '<?php echo MODULES_WEB_ROOT_DIR; ?>reporting/statistik_pengunjung_perbulan.php?tahun=<?=(date('Y')-$i)?>', 'get');"
		href="#"><?=(date('Y')-$i)?></a>
		</strong></td>
  	<?
	}
	?>
  </tr>
  <? 
  	$i=0;
	foreach($statistik_pengunjung as $item){ 
	?>
  <tr>
    <td><?=$member_type[$item['id']]?></td>
    <td class="data"><?=($item[date('Y')-5]=='')?0:$item[date('Y')-5]?></td>
    <td class="data"><?=($item[date('Y')-4]=='')?0:$item[date('Y')-4]?></td>
    <td class="data"><?=($item[date('Y')-3]=='')?0:$item[date('Y')-3]?></td>
    <td class="data"><?=($item[date('Y')-2]=='')?0:$item[date('Y')-2]?></td>
    <td class="data"><?=($item[date('Y')-1]=='')?0:$item[date('Y')-1]?></td>
    <td class="data"><?=($item[date('Y')]=='')?0:$item[date('Y')]?></td>
  </tr>
  <? 
  	$i++;
  } 
  ?>
</table>
</div>
