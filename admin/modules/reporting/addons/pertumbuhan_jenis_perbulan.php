<?php 

/*

VERSION : 3.0
CODENAME : SENAYAN
AUTHOR :
    Code and Programming : ARIE NUGRAHA (dicarve@yahoo.com)
    Database Design : HENDRO WICAKSONO (hendrowicaksono@yahoo.com) & WARDIYONO (wynerst@gmail.com)
    Customize : TOTO PRIYONO (toto_priyono@yahoo.com ) & Eddy Subratha (eddy.subratha@slims.web.id)

SENAYAN Library Automation System
Copyright (C) 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (GPL License.txt); if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* Reporting section */

// key to authenticate
define('INDEX_AUTH', '1');

// main system configuration
require '../../../../sysconfig.inc.php';

// IP based access limitation
require LIB.'ip_based_access.inc.php';
do_checkIP('smc');
do_checkIP('smc-reporting');

// start the session
require SB.'admin/default/session.inc.php';
require SB.'admin/default/session_check.inc.php';

// privileges checking
$can_read = utility::havePrivilege('reporting', 'r');
$can_write = utility::havePrivilege('reporting', 'w');

if (!$can_read) {
    die('<div class="errorBox">'.__('You don\'t have enough privileges to access this area!').'</div>');
}

$jenis = array('1'=>'Reference','2'=>'Textbook','3'=>'Fiction','4'=>'Additional','5'=>'Perpustakaan','6'=>'Reading');
$bulan = array('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des');
$tahun = $_GET['tahun'];
$str="select coll_type_id,count(item_id) as total,month(received_date) as bulan  from item where  year(received_date)=$tahun group by coll_type_id,bulan order by bulan,coll_type_id asc";
$query=$dbs->query($str);
while($data=$query->fetch_array()){
    if($data['coll_type_id']==1) {$source[1][$data['bulan']]=$data['total']; $source[1]['id']=1;}
    if($data['coll_type_id']==2) {$source[2][$data['bulan']]=$data['total'];$source[2]['id']=2;}
    if($data['coll_type_id']==3) {$source[3][$data['bulan']]=$data['total'];$source[3]['id']=3;}
    if($data['coll_type_id']==4) {$source[4][$data['bulan']]=$data['total'];$source[4]['id']=4;}
    if($data['coll_type_id']==5) {$source[5][$data['bulan']]=$data['total'];$source[5]['id']=5;}
    if($data['coll_type_id']==6) {$source[6][$data['bulan']]=$data['total'];$source[6]['id']=6;}
}
ob_start();
?>
<fieldset>
    <div class="per_title">
      <h2><?php echo __('Pertambahan Koleksi'); ?></h2>
    </div>
    <div class="sub_section">
    <h4>Berdasarkan Jenis Koleksi Pada Tahun <?php echo  $tahun ?></h4>
    <table width="100%" id="dataListPrinted" cellpadding="5" cellspacing="0" >
    <thead>
    <tr class="dataListHeader" style="font-weight: bold; cursor: pointer; background-color: rgb(49, 53, 62);">
    <td>Asal Koleksi</td>
    <?php for($i=1;$i<=12;$i++) { ?>
    <td>
    <?php echo $bulan[$i-1] ?>
    </td>
    <?php } ?>
    </tr>
    </thead>
    <tbody>
    <?php $i=0; foreach($source as $item) { ?>
    <tr>
        <td><?=$jenis[$item['id']]?></td>
        <td class="alterCell"><?php echo @($item[1]=='')?0:$item[1]?></td>
        <td class="alterCell2"><?php echo @($item[2]=='')?0:$item[2]?></td>
        <td class="alterCell" ><?php echo @($item[3]=='')?0:$item[3]?></td>
        <td class="alterCell2"><?php echo @($item[4]=='')?0:$item[4]?></td>
        <td class="alterCell"><?php echo @($item[5]=='')?0:$item[5]?></td>
        <td class="alterCell2"><?php echo @($item[6]=='')?0:$item[6]?></td>
        <td class="alterCell"><?php echo @($item[7]=='')?0:$item[7]?></td>
        <td class="alterCell2"><?php echo @($item[8]=='')?0:$item[8]?></td>
        <td class="alterCell"><?php echo @($item[9]=='')?0:$item[9]?></td>
        <td class="alterCell2"><?php echo @($item[10]=='')?0:$item[10]?></td>
        <td class="alterCell" ><?php echo @($item[11]=='')?0:$item[11]?></td>
        <td class="alterCell2"><?php echo @($item[12]=='')?0:$item[12]?></td>

    </tr>
    <?php $i++; } ?>
    </tbody>
    </table>
    </div>

    <br>
    <a class="printReport button" onclick="window.history.go(-1)" href="#">Kembali</a>    
    <br>
    <br> 
    </div>

</fieldset>
<?php 
$content = ob_get_clean();
// include the page template
require SB.'/admin/'.$sysconf['admin_template']['dir'].'/printed_page_tpl.php';

?>